package com.chadr.project1

import java.io.Serializable

data class Picture(val imageCaption:String,
                   val imageDate: String,
                   val imageDescription: String):Serializable
