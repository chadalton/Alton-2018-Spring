package com.chadr.project1

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.support.annotation.RequiresApi
import android.widget.ImageView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val CAMERA_CODE = 0

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        cameraButton.setOnClickListener {
            val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

            if(cameraIntent.resolveActivity(packageManager) != null){
                startActivityForResult(cameraIntent, CAMERA_CODE)
            }
        }

        val displayNWButton:ImageView = findViewById(R.id.displayNW)
        val displayNEButton:ImageView = findViewById(R.id.displayNE)
        val displaySWButton:ImageView = findViewById(R.id.displaySW)
        val displaySEButton:ImageView = findViewById(R.id.displaySE)
        
        displayNWButton.setOnClickListener{
            val intent = Intent(this, PictureDisplay::class.java)
            startActivity(intent)
        }
        displayNEButton.setOnClickListener{
            val intent = Intent(this, PictureDisplay::class.java)
            startActivity(intent)
        }
        displaySWButton.setOnClickListener{
            val intent = Intent(this, PictureDisplay::class.java)
            startActivity(intent)
        }
        displaySEButton.setOnClickListener{
            val intent = Intent(this, PictureDisplay::class.java)
            startActivity(intent)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode){
            CAMERA_CODE -> {
                if((resultCode == Activity.RESULT_OK) && (data != null)){
                        displayNW.setImageBitmap(data.extras.get("data") as Bitmap)
                        displayNE.setImageBitmap(data.extras.get("data") as Bitmap)
                        displaySW.setImageBitmap(data.extras.get("data") as Bitmap)
                        displaySE.setImageBitmap(data.extras.get("data") as Bitmap)
                }
            }
            else -> {
                Toast.makeText(this, "Unrecognized request code...", Toast.LENGTH_SHORT).show()
            }
        }
    }
}
