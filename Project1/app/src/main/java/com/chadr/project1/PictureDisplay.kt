package com.chadr.project1

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_picture_display.*

class PictureDisplay : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picture_display)
        if(intent.hasExtra("displayNW")){
        }

        saveButton.setOnClickListener{
            var imageCaption = captionEditText.text.toString()
            var imageDate = dateEditText.text.toString()
            var imageDescription = descriptionEditText.text.toString()

            captionEditText.setText(imageCaption)
            dateEditText.setText(imageDate)
            descriptionEditText.setText(imageDescription)
        }
    }
}
