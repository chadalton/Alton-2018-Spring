package com.chadr.test

import android.media.MediaPlayer
import android.support.v4.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import kotlinx.android.synthetic.main.fragment_home.*
import java.util.concurrent.CyclicBarrier


/**
 * Created by chadr on 5/2/2018.
 */


class HomeActivity : Fragment() {

    private lateinit var mp1:MediaPlayer
    private lateinit var mp2:MediaPlayer
    private lateinit var mp3:MediaPlayer
    private lateinit var mp4:MediaPlayer
    private lateinit var mp5:MediaPlayer
    private lateinit var mp6:MediaPlayer
    private lateinit var mp7:MediaPlayer
    private lateinit var mp8:MediaPlayer
    private lateinit var mp9:MediaPlayer
    private lateinit var mp1Btn:Button
    private lateinit var mp2Btn:Button
    private lateinit var mp3Btn:Button
    private lateinit var mp4Btn:Button
    private lateinit var mp5Btn:Button
    private lateinit var mp6Btn:Button
    private lateinit var mp7Btn:Button
    private lateinit var mp8Btn:Button
    private lateinit var mp9Btn:Button
    private lateinit var queueBtn:Button
    private lateinit var launchBtn:Button
    private var position: Int = 0
    private lateinit var soundsToLaunch: ArrayList<MediaPlayer>
    private lateinit var syncSoundLaunch: CyclicBarrier
    private var isQueue: Boolean = false
    private lateinit var sounds: ArrayList<MediaPlayer>

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater!!.inflate(R.layout.fragment_home, container, false)
        mp1Btn = view.findViewById(R.id.main_msynth)
        mp2Btn = view.findViewById(R.id.main_synth)
        mp3Btn = view.findViewById(R.id.main_pad)
        mp4Btn = view.findViewById(R.id.main_drums)
        mp5Btn = view.findViewById(R.id.main_percussion)
        mp6Btn = view.findViewById(R.id.main_fx)
        mp7Btn = view.findViewById(R.id.main_kick)
        mp8Btn = view.findViewById(R.id.main_hihat)
        mp9Btn = view.findViewById(R.id.main_snare)
        queueBtn = view.findViewById(R.id.main_queue)
        launchBtn = view.findViewById(R.id.main_launch)

        mp1Btn.setOnClickListener {
            if (isQueue) {
                soundsToLaunch.add(mp1)
            } else {
                this.playPauseSound(mp1)
            }
        }
        mp2Btn.setOnClickListener {
            if (isQueue) {
                soundsToLaunch.add(mp2)
            } else {
                this.playPauseSound(mp2)
            }
        }
        mp3Btn.setOnClickListener {
            if (isQueue) {
                soundsToLaunch.add(mp3)
            } else {
                this.playPauseSound(mp3)
            }
        }
        mp4Btn.setOnClickListener {
            if (isQueue) {
                soundsToLaunch.add(mp4)
            } else {
                this.playPauseSound(mp4)
            }
        }
        mp5Btn.setOnClickListener {
            if (isQueue) {
                soundsToLaunch.add(mp5)
            } else {
                this.playPauseSound(mp5)
            }
        }
        mp6Btn.setOnClickListener {
            if (isQueue) {
                soundsToLaunch.add(mp6)
            } else {
                this.playPauseSound(mp6)
            }
        }
        mp7Btn.setOnClickListener {
            if (isQueue) {
                soundsToLaunch.add(mp7)
            } else {
                this.playPauseSound(mp7)
            }
        }
        mp8Btn.setOnClickListener {
            if (isQueue) {
                soundsToLaunch.add(mp8)
            } else {
                this.playPauseSound(mp8)
            }
        }
        mp9Btn.setOnClickListener {
            if (isQueue) {
                soundsToLaunch.add(mp9)
            } else {
                this.playPauseSound(mp9)
            }
        }
        queueBtn.setOnClickListener() {
            if (isQueue) {
                isQueue = false
                main_queue.setText("QUEUE")
            } else if (!isQueue) {
                isQueue = true
                main_queue.setText("NORMAL")
            }
        }
        launchBtn.setOnClickListener() {
            if (soundsToLaunch.size != 0) {
                syncSoundLaunch = CyclicBarrier(soundsToLaunch.size)

                for (sound in soundsToLaunch)
                    syncSoundLaunch(sound, syncSoundLaunch)
            }
            if (isQueue) {
                isQueue = false
                main_queue.setText("QUEUE")
            }
        }

        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        soundsToLaunch = ArrayList()
        sounds = ArrayList()

        mp1 = MediaPlayer.create(this.context, R.raw.msynth1120a)
        mp2 = MediaPlayer.create(this.context, R.raw.synth1120a)
        mp3 = MediaPlayer.create(this.context, R.raw.pad2120a)
        mp4 = MediaPlayer.create(this.context, R.raw.drums2120a)
        mp5 = MediaPlayer.create(this.context, R.raw.percussion1120a)
        mp6 = MediaPlayer.create(this.context, R.raw.fx1120a)
        mp7 = MediaPlayer.create(this.context, R.raw.kick1120a)
        mp8 = MediaPlayer.create(this.context, R.raw.hihat1120a)
        mp9 = MediaPlayer.create(this.context, R.raw.snare1120a)
    }

    override fun onDestroy() {
        super.onDestroy()
        mp1.release()
        mp2.release()
        mp3.release()
        mp4.release()
        mp5.release()
        mp6.release()
        mp7.release()
        mp8.release()
        mp9.release()
    }

    private fun playPauseSound(mp: MediaPlayer) {
        if (mp.isPlaying) {
            stopSound(mp)
        } else {
            mp.start()
        }
    }

    private fun stopSound(mp: MediaPlayer) {
        mp.pause()
        position = 0
        mp.seekTo(0)
    }

    private fun syncSoundLaunch(mp: MediaPlayer, cb: CyclicBarrier) {
        StartThread(mp, cb).start()
    }
}