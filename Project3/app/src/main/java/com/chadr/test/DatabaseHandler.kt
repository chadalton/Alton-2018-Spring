package com.chadr.test

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.widget.Toast

/**
 * Created by chadr on 4/5/2018.
 */

val DB_NAME = "P3DB"
val TABLE_NAME = "SoundPacks"
val COL_ID = "id"
val COL_TITLE = "title"
val COL_INFO = "soundPackInfo"

class DatabaseHandler(var context: Context) : SQLiteOpenHelper(context, DB_NAME, null, 1) {
    override fun onCreate(db: SQLiteDatabase?) {
        val createTable: String =
                "CREATE TABLE $TABLE_NAME($COL_ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "$COL_TITLE VARCHAR(256), " +
                        "$COL_INFO VARCHAR(256))"
        db?.execSQL(createTable)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db!!.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME)
        onCreate(db)
    }

    fun insertSoundPack(soundPack: SoundPack) {
        val db = this.writableDatabase
        var cv = ContentValues()
        cv.put(COL_TITLE, soundPack.title)
        cv.put(COL_INFO, soundPack.soundPackInfo)
        var result = db.insert(TABLE_NAME, null, cv)

        if (result.equals(-1)) {
            Toast.makeText(context, "Failed inserting to DB", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(context, "Successfully inserted to DB", Toast.LENGTH_SHORT).show()
        }
    }

    fun getSoundPacks(): ArrayList<SoundPack> {
        var list: ArrayList<SoundPack> = ArrayList()

        val db = this.readableDatabase
        val query = "SELECT * FROM " + TABLE_NAME
        val result = db.rawQuery(query, null)

        while (result.moveToNext()) {
            var soundPack = SoundPack()
            soundPack.title = result.getString(result.getColumnIndex(COL_TITLE))
            soundPack.soundPackInfo = result.getString(result.getColumnIndex(COL_INFO))
            list.add(soundPack)
        }

        result.close()
        db.close()
        return list
    }
}