package com.chadr.test


import android.media.MediaPlayer
import java.util.concurrent.CyclicBarrier


/**
 * Created by chadr on 5/2/2018.
 */


class StartThread(private val mp:MediaPlayer, private val cb:CyclicBarrier) : Thread()
{
    override fun run() {
        cb.await()
        mp.start()
    }
}
