package com.chadr.test


/**
 * Created by chadr on 5/5/2018.
 */
class SoundPack {
    var id:Int = 0
    var title:String = ""
    var soundPackInfo:String = ""

    constructor()

    constructor(title:String, soundPackInfo:String){
        this.title = title
        this.soundPackInfo = soundPackInfo
    }
}