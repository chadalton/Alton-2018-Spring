package com.chadr.test


import android.content.Context
import android.support.v4.app.Fragment
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.chadr.project1.CustomAdapter
import kotlinx.android.synthetic.main.fragment_about.view.*
import java.io.File


/**
 * Created by chadr on 5/2/2018.
 */

class AboutActivity: Fragment(){

    private lateinit var db:DatabaseHandler

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        deleteDatabase(context)
        db = DatabaseHandler(context)
        createInitialData(db)
        val soundPacks = db.getSoundPacks()
        val view:View = inflater!!.inflate(R.layout.fragment_about, container, false)
        val recyclerView : RecyclerView = view.recyclerView
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = CustomAdapter(soundPacks, {soundPack:SoundPack -> soundPackClicked(soundPack)})
        return view
    }

    private fun soundPackClicked(soundPack: SoundPack){
        Toast.makeText(context, "soundPackClicked", Toast.LENGTH_SHORT).show()
    }

    private fun deleteDatabase(context:Context){
        val dbName = "P3DB"
        val dbFile:File = (context.getDatabasePath(dbName))

        if(dbFile.exists()){
            context.deleteDatabase(dbName)
        }
    }

    private fun createInitialData(db:DatabaseHandler){
        db.insertSoundPack(SoundPack("Tropical", "Bpm: 120 Key: F"))
        db.insertSoundPack(SoundPack("Pop", "Bpm: 100 Key: D"))
        db.insertSoundPack(SoundPack("Hip Hop", "Bpm: 90 Key: D"))
        db.insertSoundPack(SoundPack("Rap", "Bpm: 90 Key: D"))
    }
}
