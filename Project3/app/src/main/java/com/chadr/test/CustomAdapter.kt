package com.chadr.project1

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.chadr.test.R
import com.chadr.test.SoundPack
import kotlinx.android.synthetic.main.list_item.view.*

/**
 * Created by chadr on 4/4/2018.
 */

class CustomAdapter(private val soundPackList: ArrayList<SoundPack>,
                    private val clickListener: (SoundPack) -> Unit) : RecyclerView.Adapter<CustomViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val recyclerView = layoutInflater.inflate(R.layout.list_item, parent, false)

        return CustomViewHolder(recyclerView)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        var soundPack = soundPackList[position]
        (holder).bind(soundPack, clickListener)
    }

    override fun getItemCount(): Int {
        return soundPackList.size
    }
}

class CustomViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(soundPack: SoundPack, clickListener: (SoundPack) -> Unit) {
        itemView.list_title.text = soundPack.title
        itemView.list_sound_info.text = soundPack.soundPackInfo
        itemView.setOnClickListener { clickListener(soundPack)}
    }
}


