package com.chadr.project1

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.view_pictures.view.*

/**
 * Created by chadr on 4/4/2018.
 */

class ViewPictureFragment : Fragment(){

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val db = DatabaseHandler(this.context)
        val pictures = db.getPictureObjects()
        val view: View = inflater!!.inflate(R.layout.view_pictures, container, false)
        val recyclerView = view.recyclerView as RecyclerView
        recyclerView.layoutManager = LinearLayoutManager(this.context)
        recyclerView.adapter = ViewPictureAdapter(pictures, {pictureItem: Picture -> pictureItemClicked(pictureItem)})
        return view
    }

    private fun pictureItemClicked(pictureitem: Picture){
        // Create Intent
        val intent = Intent(activity.baseContext, EditPictureActivity().javaClass)

        // Pack Data
        intent.putExtra("Caption", pictureitem.imageCaption)
        intent.putExtra("Date", pictureitem.imageDate)
        intent.putExtra("Description", pictureitem.imageDate)
        intent.putExtra("Image", pictureitem.image)
        intent.putExtra("ID", pictureitem.id)

        // Start Activity
        activity.baseContext.startActivity(intent)
    }
}
