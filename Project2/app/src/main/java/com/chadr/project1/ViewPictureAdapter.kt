package com.chadr.project1

import android.graphics.BitmapFactory
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.picture_row.view.*

/**
 * Created by chadr on 4/4/2018.
 */

class ViewPictureAdapter(private val pictureList: ArrayList<Picture>,
                         private val clickListener: (Picture) -> Unit) : RecyclerView.Adapter<PictureViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PictureViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val recyclerView = layoutInflater.inflate(R.layout.picture_row, parent, false)

        return PictureViewHolder(recyclerView)
    }

    override fun onBindViewHolder(holder: PictureViewHolder, position: Int) {
        var pictureObject = pictureList[position]
        val bitMap = BitmapFactory.decodeByteArray(pictureObject.image, 0, pictureObject.image.size, BitmapFactory.Options())
        holder.image.setImageBitmap(bitMap)
        (holder).bind(pictureObject, clickListener)
    }

    override fun getItemCount(): Int {
        return pictureList.size
    }
}

class PictureViewHolder(pictureView: View) : RecyclerView.ViewHolder(pictureView) {
    var image = pictureView.recyclerImg!!
    fun bind(picture: Picture, clickListener: (Picture) -> Unit) {
        itemView.recycler_date.text = "Date" + picture.imageDate
        itemView.recycler_caption.text = "Caption" + picture.imageCaption
        itemView.recycler_desc.text = "Description" + picture.imageDescription
        itemView.setOnClickListener { clickListener(picture)}
    }
}


