package com.chadr.project1

/**
 * Created by chadr on 4/5/2018.
 */

class Picture{
    var id : Int = 0
    var size : Int = 0
    var imageCaption : String = ""
    var imageDate : String = ""
    var imageDescription : String = ""
    var image : ByteArray = ByteArray(size)

    constructor()

    constructor(size:Int, imageCaption:String, imageDate:String, imageDescription:String, image:ByteArray){
        this.size = size
        this.imageCaption = imageCaption
        this.imageDate = imageDate
        this.imageDescription = imageDescription
        this.image = image
    }
}