package com.chadr.project1

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.support.annotation.RequiresApi
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import android.graphics.Bitmap.CompressFormat
import java.io.ByteArrayOutputStream


class MainActivity : AppCompatActivity() {

    private val cameraCode = 0
    private val manager = supportFragmentManager

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        cameraButton.setOnClickListener {
            startCamera()
        }

        fragmentButton.setOnClickListener({
            showViewPicturesFragment()
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        insertCameraPicInDB(requestCode, resultCode, data)
    }

    private fun startCamera() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (cameraIntent.resolveActivity(packageManager) != null) {
            startActivityForResult(cameraIntent, cameraCode)
        }
    }

    private fun insertCameraPicInDB(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            cameraCode -> {
                if ((resultCode == Activity.RESULT_OK) && (data != null)) {
                    val image = data.extras.get("data") as Bitmap
                    var picture = Picture(
                            image.byteCount,
                            "",
                            "",
                            "",
                            getBytes(image))
                    var db = DatabaseHandler(this)
                    db.insertPicture(picture)
                }
            }
            else -> {
                Toast.makeText(this, "Unrecognized request code...", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun getBytes(bitmap: Bitmap): ByteArray {
        val stream = ByteArrayOutputStream()
        bitmap.compress(CompressFormat.PNG, 100, stream)
        return stream.toByteArray()
    }

    private fun showViewPicturesFragment(){
        val transaction = manager.beginTransaction()
        val fragment = ViewPictureFragment()
        transaction.replace(R.id.fragment_holder, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}
