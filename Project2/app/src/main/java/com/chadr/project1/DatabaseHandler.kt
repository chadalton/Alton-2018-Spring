package com.chadr.project1

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.widget.Toast

/**
 * Created by chadr on 4/5/2018.
 */

val DB_NAME = "P2DB"
val TABLE_NAME = "Photos"
val COL_ID = "id"
val COL_IMAGE_CAPTION = "imageCaption"
val COL_IMAGE_DATE = "imageDate"
val COL_IMAGE_DESCRIPTION = "imageDescription"
val COL_IMAGE = "photo"

class DatabaseHandler(var context: Context) : SQLiteOpenHelper(context,DB_NAME,null,1){
    override fun onCreate(db: SQLiteDatabase?) {
        val createTable = "CREATE TABLE " + TABLE_NAME + "(" +
                COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                COL_IMAGE_CAPTION + " VARCHAR(256)," +
                COL_IMAGE_DATE + " VARCHAR(256)," +
                COL_IMAGE_DESCRIPTION + " VARCHAR(256)," +
                COL_IMAGE + " BLOB)"
        db?.execSQL(createTable)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db!!.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME)
        onCreate(db)
    }

    fun insertPicture(picture:Picture){
        val db = this.writableDatabase
        var cv = ContentValues()
        cv.put(COL_IMAGE_CAPTION, picture.imageCaption)
        cv.put(COL_IMAGE_DATE, picture.imageDate)
        cv.put(COL_IMAGE_DESCRIPTION, picture.imageDescription)
        cv.put(COL_IMAGE, picture.image)
        var result = db.insert(TABLE_NAME, null, cv)

        if(result.equals(-1)){
            Toast.makeText(context, "Failed inserting to DB", Toast.LENGTH_SHORT).show()
        }else{
            Toast.makeText(context, "Successfully inserted to DB", Toast.LENGTH_SHORT).show()
        }
    }

    fun getPictureObjects() : ArrayList<Picture>{
        var list : ArrayList<Picture> = ArrayList()

        val db = this.readableDatabase
        val query = "SELECT * FROM " + TABLE_NAME
        val result = db.rawQuery(query, null)

        if(result.moveToFirst()){
            while(result.moveToNext()){
                var picture = Picture()
                picture.imageDescription = result.getString(result.getColumnIndex(COL_IMAGE_DESCRIPTION))
                picture.imageDate = result.getString(result.getColumnIndex(COL_IMAGE_DATE))
                picture.imageCaption = result.getString(result.getColumnIndex(COL_IMAGE_CAPTION))
                picture.image = result.getBlob(result.getColumnIndex(COL_IMAGE))
                list.add(picture)
            }
        }

        result.close()
        db.close()
        return list
    }

    fun deletePicture(id: Integer, db: SQLiteDatabase?){
        val deleteQuery = "DELETE FROM " + TABLE_NAME + " WHERE " + COL_ID + "=" + "'" + id + "';"
        db?.execSQL(deleteQuery)
    }
}