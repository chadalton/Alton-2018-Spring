package chadalton.com.homework1

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        concatBtn.setOnClickListener {
            val string1 = val1.text.toString()
            val string2 = val2.text.toString()
            val result = string1 + " " + string2

            display.text = result
        }

        addBtn.setOnClickListener {
            val num1 = val1.text.toString()
            val num2 = val2.text.toString()
            val result = num1.toInt() + num2.toInt()

            display.text = result.toString()
        }
    }
}
